﻿using System;

namespace week5_ex24_d
{
    class Program
    {
        static void Main(string[] args)
        {
            //Determines numbers
            Console.WriteLine(Method(3,3,2));
        }

        static string Method(int num1, int num2, int num3)
        {
            //multiplies numbers
            return $"Sum of the 3 numbers multiplied is {num1*num2*num3}";
        }
    }
}
